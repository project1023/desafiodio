enum class Nivel { BASICO, INTERMEDIARIO, DIFICIL }

data class Usuario(val name: String)

data class ConteudoEducacional(val nome: String, val duracao: Int = 60)

data class Formacao(val nome: String, var conteudos: List<ConteudoEducacional>) {

    val inscritos = mutableListOf<Usuario>()

    fun matricular(usuario: Usuario) {
        inscritos.add(usuario)
    }


}

fun verifica(f: Formacao) {
    when (f.nome) {
        "Inglês I" -> Nivel.BASICO
        "Inglês II" -> Nivel.INTERMEDIARIO
        "Inglês III" -> Nivel.DIFICIL
    }
}

fun main() {

    val usuario = Usuario("Zoe")

    val conteudo1 = ConteudoEducacional("Hello")
    val conteudo2 = ConteudoEducacional("Basic")
    val formacao = Formacao("Inglês I", listOf(conteudo1, conteudo2)).also {
        verifica(it)
        it.matricular(usuario)

    }

    println("Os Alunos inscritos na formação ${formacao.nome} são ${formacao.inscritos}")

}